import { Heading, Flex, Text, Box, Link, Container } from '@chakra-ui/react';
import Image from 'next/image';
import styles from '../styles/Home.module.css'
import { AnimatePresence } from 'framer-motion';
import Section from '../components/section';
import Navbar from '../components/navbar';
import scrollStyles from '../styles/scrollbar.module.css';


export default function OrderMePhoto() {

  return (
    <Box className={styles.home}>
      <Section>
        <Navbar />
        <Flex
          flexDir='column'
          justify={'center'}
          align={'center'}
          minH='50%'
          height='auto'

        >
          <AnimatePresence>
            <Heading mt='10vh'>
              Order Me Photo
            </Heading>
            <Image src='/order-me.png' height={500} width={800} />
            < Flex
              flexDir='column'
              align={'center'}
              minH='50%'
              height='auto'
              width='80%'
            >
              <Heading mt={8} size={'lg'}>
                Description
              </Heading>
              <Text mt={4}>
                Status: In development
              </Text>
              <Text mt={5}>
                This application is designed to work on a raspberry pi. The idea behind is that the Pi host the fronend backend and database as a local server. So that if there is no internet at the location any machine that is connected locally to the Pi will be able to use the application.
                This would allow the photography the ablity to quick load photos and the customer to select and place an order on site.
              </Text>
              <Text>
              </Text>
              <Heading mt={8} size={'lg'}>
                Future Plans
              </Heading>
              <Text mt={4}>
                Create another version that is fully hosted and used as a web application and extend the use beyond photos
              </Text>
            </Flex>

            <Flex flexDir={'column'} mt={5} justify={'center'} align={'center'} width={'80%'}>
              <Flex justify={'center'} width={'80%'}>
                <Heading size={'lg'}>
                  Follow along with the creation
                </Heading>
              </Flex>
              <Flex overflowX={'scroll'} width={'80%'} className={scrollStyles.hide} marginY={5} justifyContent={'space-around'} >
                <Flex flexDir={'column'} mt={5} justify={'center'} align={'center'} >
                  <Box>
                    <Link href='https://youtu.be/kfmoN590FD0' isExternal >

                      <Image src='/youtube.png' alt='video thumbnail' height={100} width={100} />
                    </Link>
                  </Box>
                  <Box>
                    <Text>
                      Photosite using Next.js and Chakra-ui.
                    </Text>
                  </Box>
                </Flex>
                <Flex flexDir={'column'} mt={5} justify={'center'} align={'center'} >
                  <Box>
                    <Link href='https://gitlab.com/jboothwebdev' isExternal >
                      <Image src='/gitlab.png' alt='video thumbnail' height={100} width={100} />
                    </Link>
                  </Box>
                  <Box>
                    <Text>
                      For source code click here
                    </Text>
                  </Box>
                </Flex>
              </Flex>
            </Flex>
          </AnimatePresence>
        </Flex >
      </Section >
    </Box >
  )

} 
