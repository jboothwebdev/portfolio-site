import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Flex, AspectRatio, Link, Heading, Text, UnorderedList, ListItem } from "@chakra-ui/react";
import Navbar from "../components/navbar";
import styles from "../styles/Home.module.css"

export default function OnBugdet() {

  return (
    <Flex minH={"100vh"}
      flexDir={"column"}
      justify={'center'}
      alignItems={"center"}
      className={styles.home}
    >
    <Navbar/>
      <Heading>
        onBudget
      </Heading>
      <Link href="https://onbudget.vercel.app/" color="blue.300" isExternal>
        Go to application <ExternalLinkIcon mx='2px'/>
     </Link>

      <Flex
        mt={10}
        flexDir={"column"}
        justify={"center"}
        align={"center"}
      >
        <Heading mb={4} fontSize='xl'>
          Description
        </Heading>
        <Text>
          A Budget web application that is focused on being simple and easy to use.
        </Text>
        <Text maxW={"60%"} marginY={4}>
          No linking to bank accounts, or having to dig through mountains of categories to label your transactions. Since you create everything from scratch you can control how complicated or simple
          your budget will be.
        </Text>

        <Text 
            maxW={"60%"} 
            marginY={4}
        >
            Focuses on the monthly budgets with at create as you go mindset. Also allows for overviews as well as detail views of the months, categories and transactions.
            A newly added feature allows you create savings or payoff plans that take a goal and allows you to select a duration and will then show you the amount that you will need set aside for each interation to make that goal. 
        </Text>

        <Heading 
            mt={10}
            mb={4}
            fontSize='xl'
        >
          Future Plans
        </Heading>
        <UnorderedList
        maxW={"60%"}
        marginY={4}
        >
        <ListItem>
            Update the detail views to provide more information.
        </ListItem>
        <ListItem>
            Looking into providing a way to track investments. 
        </ListItem>
        <ListItem>
            Expanding ability of the calculator.
        </ListItem>
        </UnorderedList>
      </Flex>
    </Flex>
  )
}



