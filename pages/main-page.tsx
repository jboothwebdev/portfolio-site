import { Flex, Text } from '@chakra-ui/react'
import { motion } from 'framer-motion';
import styles from '../styles/Home.module.css'
import Navbar from '../components/navbar'
import Banner from '../components/banner'
import About from '../components/about'
import Projects from '../components/projects'
import Section from '../components/section'


export default function Main() {
  return (
    <Flex
      className={styles.home}
      flexDir='column'
      justify={'center'}
    >
      <Navbar />
      <Flex
        position={'relative'}
        direction="row"
        justify="center"
        align="center"
        h="37%"
        w="100%"
        overflow="hidden"
        top='7vh'
      >
        <motion.div
          initial={{ y: -500 }}
          animate={{ y: 0 }}
          transition={{ duration: 0.5 }}
        >
          <Banner />
        </motion.div>
        <motion.div
          initial={{ x: 1000 }}
          animate={{ x: 0 }}
          transition={{
            duration: 0.5,
          }}
        >
          <Text as="strong" fontSize="4xl" w="40vw" marginRight="10vw">
            Create Through Code
          </Text>
        </motion.div>
      </Flex>
      <Section>
        <About />
      </Section>
      <Flex
        flexDir='column'
        justify='center'
        align='center'
      >
        <Projects />
      </Flex>
    </Flex>
  )
};
