import TutorialNavbar from '../../components/tutorialNavbar'
import { Flex } from "@chakra-ui/react";
import styles from '../../styles/Home.module.css'
import {useState} from "react";
import TutorialSidebar from "../../components/tutorialSidebar";


export default function TutorialHome() {
  const [sidebar, setSidebar] = useState(false)

  function updateSidebar() {
    setSidebar(!sidebar);
  }

  return (
    <Flex
        minH={"100vh"}
        className="tutorial-home-page"
    >
      <TutorialNavbar setSideBar={() => updateSidebar}/>
      <Flex 
      position={"relative"} 
      top={'4vh'}
      width={"100%"}
      height={"100%"}
      >
        {sidebar ? <TutorialSidebar /> : ''}
      </Flex>
    </Flex>
  );
}
