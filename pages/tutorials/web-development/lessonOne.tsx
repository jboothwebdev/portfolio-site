import TutorialNavbar from '../../../components/tutorialNavbar'
import {
  Card,
  CardBody,
  CardHeader,
  Divider,
  Flex,
  Heading,
  List,
  ListItem,
  ListIcon,
  OrderedList,
  Text,
  UnorderedList,
  Code,
} from "@chakra-ui/react";
import { ChevronRightIcon, ChevronDownIcon } from '@chakra-ui/icons'
import styles from '../../../styles/Home.module.css'
import { useState } from "react";
import TutorialSidebar from "../../../components/tutorialSidebar";

export default function LessonOne() {
  const [sidebar, setSidebar] = useState(false)

  function updateSidebar() {
    setSidebar(!sidebar);
  }

  return (
    <Flex
      bg={"gray.900"}
      color={"gray.50"}
      minH={"100%"}
    >
      <TutorialNavbar setSideBar={() => updateSidebar} />
      <Flex
        position={"relative"}
        top={'5vh'}
        width={"100%"}
        justify={"space-between"}
      >
        {sidebar ? <TutorialSidebar /> : ''}
        <Flex
          flexDir={'column'}
          width={"100%"}
          minH={"100%"}
        >
          <Heading ml={3}>
            Lesson one
          </Heading>
          <Heading ml={5} size={'sm'}>
            Intro to HTML and CSS
          </Heading>

          <Divider
            mb={5}
          />

          <Heading
            ml={3}
            size={"lg"}>
            The first project
          </Heading>

          <Heading
            ml={5}
            size={"md"}>
            Overview:
          </Heading>

          <Flex flexDir={"column"} w={'80%'} m={10}>
            <List spacing={3}>
              <ListItem>
                <ListIcon as={ChevronRightIcon} color={"green.300"} />
                Task: create an informational website.
              </ListItem>
              <ListItem>
                <ListIcon as={ChevronRightIcon} color={"green.300"} />
                Tech stack: HTML and CSS.
              </ListItem>
            </List>
            <Card
              mt={5}
              variant={"outline"}
              size="sm"
            >
              <CardBody
                color={"gray.400"}
              >
                Notes: You will see some terminal commands in a block like this.
              </CardBody>
            </Card>
            <Text mt={5}>
              In these tutorials you may see something like this {'<yourFileName>'}.
              This means that you should replace this including the angle brackets with a file name of your choice. For example;
            </Text>
            <Card
              mt={5}
              variant={"outline"}
              size="sm"
            >
              <CardBody
                color={"gray.400"}
              >
                <Flex>
                  <Text color={'blue.400'}> mkdir</Text>
                  <Text ml={1}> {"<yourFileName>"} </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text
              mt={5}
            >
              becomes;
            </Text>

            <Card
              mt={5}
              variant={"outline"}
              size="sm"
            >
              <CardBody
                color={"gray.400"}
              >

                <Flex>
                  <Text color={'blue.400'}> mkdir</Text>
                  <Text ml={1}> lessonOne </Text>
                </Flex>
              </CardBody>
            </Card>

          </Flex>

          <Heading ml={5} size='md'>
            Instructions:
          </Heading>

          <Flex flexDir={"column"} w={'80%'} m={10}>
            <OrderedList>
              <ListItem>
                Check and make sure that you have your editor installed and ready to go.
              </ListItem>
              <ListItem>
                Next bring up your terminal.
              </ListItem>
              <List>
                <ListItem>
                  <ListIcon as={ChevronRightIcon} color={"green.300"} />
                  On window you can use powershell or you can install git and the git bash(recommended).
                </ListItem>
                <ListItem>
                  <ListIcon as={ChevronRightIcon} color={"green.300"} />
                  On mac nad linux just follow along.
                </ListItem>
              </List>
              <ListItem>
                Here we are going to create a new directory to house the projects we create.
              </ListItem>
            </OrderedList>

            <Text>
              Not that your terminal is open you should see something like this.
            </Text>
            <Card
              mt={5}
              variant={"outline"}
              size="sm"
            >
              <CardBody
                color={"gray.400"}
              >
                <Text
                  color={"gray.400"}
                >
                  ~
                </Text>
              </CardBody>
            </Card>

            <Text mt={5}>
              This signifies that you are in your users root directory if you type in the command
              <Code mx={3}>ls</Code> and then hit enter
            </Text>
            <Text>
              You would see a long list of files and directories of folders.
            </Text>

            <Text>
              Now we are going to create a new directory to store our coding projects in with the following command.
            </Text>

            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody color={"gray.400"}>
                <Flex>
                  <Text
                    color={"blue.400"}
                    mr={2}
                  >
                    mkdir
                  </Text>
                  <Text> coding </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text mt={3}>
              Now if you enter the <Code>ls</Code> again you should see the <Text as='i'> coding </Text> directory
            </Text>

            <Text>
              Next we need to change directory so that we are inside the coding directory. Using the
              <Code mx={2}> cd</Code>
              command
            </Text>
            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Flex>
                  <Text
                    color={"blue.400"}
                    mr={2}
                  >
                    cd
                  </Text>
                  <Text>
                    coding
                  </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text
              mt={3}
            >
              Now the terminal prompt should looke like this
            </Text>
            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Flex>
                  <Text
                    color={"blue.400"}
                    mr={2}
                  >
                    ~/coding
                  </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text
              mt={3}
            >
              We are going to do all that one more time. This time we are going to create a folder for this lesson specificaly
            </Text>
            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Flex>
                  <Text
                    color={"blue.400"}
                    mr={2}
                  >
                    mkdir
                  </Text>
                  <Text>
                    lessonOne
                  </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text
              mt={3}
            >
              This is going to be the folder that we are going to be working for awhile. So we are going to <Code mx={2}>cd</Code> in to the lessonOne folder.
            </Text>
            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Flex>
                  <Text
                    color={"blue.400"}
                    mr={2}
                  >
                    cd
                  </Text>
                  <Text>
                    lessonOne
                  </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text
              mt={5}
            >
              Excellent now you should be in the
              <Text as={'i'} mx={1}>~/coding/lessonOne</Text>
              folder.
              Now you need to open up your code editor and navigate to that folder.
              If you are using VSCode. You can enter the following command to open VSCode directly from the terminal.
            </Text>
            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Flex>
                  <Text>
                    code .
                  </Text>
                </Flex>
              </CardBody>
            </Card>

            <Text mt={3}>
              Yes you should have the period in there. This command means open VSCode with all items in this directory.
            </Text>

            <Text mt={3}>
              Now we are going to use your editor to create the first file which is going to be a HTML file by the standard naming convention this file should be called <Text as={'i'}> index </Text>. So in the project file tree viewer right click and select new file and name it <Text as={'i'}> index.html</Text>
            </Text>

            <Text mt={3}>
              From this point on the lessonOne directory is going to be called the root directory and your projects file tree should look like this.
            </Text>

            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <List>
                  <ListItem>
                    <ListIcon as={ChevronDownIcon} color={"green.400"} />
                    lessonOne
                  </ListItem>
                  <ListItem ml={5}>
                    <ListIcon as={ChevronRightIcon} color={"red.400"} />
                    index.html
                  </ListItem>
                </List>
              </CardBody>
            </Card>

            <Text mt={3}>
              Now if your IDE will let you or you can use your file explorer. Go to the index.html right click and on it and select, open with, then select your browser of choice. You should see a blank page open in your browser.
            </Text>

            <Text mt={3}>
              You should see a blank page open ui in your browser.
            </Text>

            <Text mt={3}>
              5. Add some HTML.
            </Text>

            <Text mt={3}>
              The first thing that we are going to add is what is referred to as boiler plate.
            </Text>

            <Text mt={3}>
              Copy and past this into yoer index.html file.
            </Text>

            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Text>
                  {`<html lang="en">`}
                </Text>
                <Text ml={4}>
                  {`
                    <head>
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <title></title>
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta charset="UTF-8">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <link href="css/style.css" rel="stylesheet">
                  `}
                </Text>
                <Text ml={4}>
                  {`
                  </head>
                  `}
                </Text>

                <Text ml={4}>
                  {`
                    <body>
                  `}
                </Text>
                <Text ml={4}>
                  {`
                        </body>
                  `}
                </Text>

                <Text>
                  {`
                    </html> `}
                </Text>
              </CardBody>

            </Card>

            <Text mt={3}>
              Alright our first dive into html. So lets give you some of the important information.
              HTML is a acronym for Hypertext Markup Language,
              the <em>keywords</em> are referred to as tags and are enclosed in the angle brackets <Text color={'blue.400'} as='b'>{'<>'}</Text>.

              Generally speaking there is a opening and a closing tag.
              In your editor you see that first tag the <Text color={"blue.400"} as="b">{`<html>`}</Text> this is the opening tag. Then at the bottom you see the <Text color={"blue.400"} as="b">{`</html>`}</Text> this is the closing tag. The closing tag will always have that `/` before the tag name to signify that that is the closing tag.
            </Text>

            <Text mt={3}>
              Now also notice that all the other tags are inside of the <Text color={"blue.400"} as="b">{`<html>`}</Text> tags. This is because the html tag is the root tag. We have some different ways to look at this. There is the common tree method where in this case the <Text color={"blue.400"} as="b">{`<html>`}</Text> is the base of the tree and everything else is branches. You also have the parent-child method where the html tag is the oldest parent and it has childern and those children have grandchildren and so forth.
            </Text>

            <Text mt={3}>
              But as the same time you also have to think of it in terms of the browser. Right now when we opened that file we got a tab with name <Text as="i">index.html. </Text> The url is the route to the file and a blank white page. right now that entire setup is the <Text color={"blue.400"} as="b">{`<html>`}</Text> tag. as we look closer to our html code we see that the next children are a <Text color={"blue.400"} as="b">{`<head>`}</Text> and a <Text color={"blue.400"} as="b">{`<body>`}</Text> tags.
              The <Text color={"blue.400"} as="b">{`<head>`}</Text>tag has further children, but the <Text color={"blue.400"} as="b">{`<body>`}</Text> tag is blank.
              Let's make some changes so we can visualise this structure.
            </Text>

            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Text>
                  {`<html lang="en">`}
                </Text>
                <Text ml={4}>
                  {`
                    <head>
                  `}
                </Text>
                <Text ml={8}>
                  {`<!-- change this -->`}
                </Text>
                <Text ml={8}>
                  {`
                    <title> Lesson One</title>
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta charset="UTF-8">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <link href="css/style.css" rel="stylesheet">
                  `}
                </Text>
                <Text ml={4}>
                  {`
                  </head>
                  `}
                </Text>

                <Text ml={4}>
                  {`
                    <body>
                  `}
                </Text>
                <Text ml={4}>
                  {`
                        </body>
                  `}
                </Text>

                <Text>
                  {`
                    </html> `}
                </Text>
              </CardBody>
            </Card>

            <UnorderedList mt={3}>
              <ListItem>
                First notice the {`<!-- -->`}
              </ListItem>
              <ListItem ml={3}>
                This is a comment in html it is a everything in between the opening and close tag is not executable or in this case will not show up in the browser this is a good way to leave notes and hints in your code. You will see this a lot and the standard practice is to put it on the line above the code that it referances. Here it means to change the line of code below it.
              </ListItem>
              <ListItem>
                Second inbetween the opening <Text color={'blue.400'} as='b'> {`<title>`}</Text> and the closing <Text color={'blue.400'} as='b'> {`</title>`}</Text> we add the text 'Lesson One'.
              </ListItem>
              <ListItem>
                Now save your changes and go back to your browser and refresh the page.
              </ListItem>
            </UnorderedList>

            <Text mt={3}>
              Look at the tab of your page. It should have changed from <Text as='i'>index.html</Text> to "Lesson One".
            </Text>

            <Text mt={3}>
              The head tag of HTML contains information that the browser and other sites use to determine get information about your page one of those is the title for your page that gets displayed in the tab. By adding the text in the title tag we told the browser that we want the tab of this page to be titled 'lesson one'. We will go over more of the head tag later on.
            </Text>

            <Text mt={3}>
              For now lets move into the body. Add the following changes tothe body tag.
            </Text>

            <Card
              variant={"outline"}
              size="sm"
              mt={5}
            >
              <CardBody>
                <Text>
                  {`<html lang="en">`}
                </Text>
                <Text ml={4}>
                  {`
                    <head>
                  `}
                </Text>

                <Text ml={8}>
                  {`
                    <title> Lesson One</title>
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta charset="UTF-8">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                  `}
                </Text>
                <Text ml={8}>
                  {`
                    <link href="css/style.css" rel="stylesheet">
                  `}
                </Text>
                <Text ml={4}>
                  {`
                  </head>
                  `}
                </Text>
                <Text ml={4}>
                  {`
                    <body>
                  `}
                </Text>
                <Text ml={8}>
                  {`<!-- change this -->`}
                </Text>
                <Text ml={8}>
                  {`<h1> Hello Body </h1>`}
                </Text>
                <Text ml={4}>
                  {`
                        </body>
                  `}
                </Text>

                <Text>
                  {`
                    </html> `}
                </Text>
              </CardBody>
            </Card>

            <Text mt={3}>
              Save and refresh.
            </Text>

            <Text mt={3}>
              Now you should notice in the main window.
              It is no longer a blank page.
              But it has the text Hello body in it.
              That brings us to a third way to look at your html code and that is a series of boxes.
              The <Text color={"blue.400"} as='b'>{`<html>`}</Text> is the always the outermost box.
              The <Text color={"blue.400"} as="b">{`<body>`}</Text> tag is a smaller box in the html box but is the largest box of the main body of the window.
              The window being the area of the browser the user interacts with the contents of your page.
              We will add more and more boxes to the body and more boxes in those and that is what will give structure to the website.
              Then we will use CSS to style and put the boxes where we want them to be.
            </Text>

            <Text mt={3}>
              But before we move on lets talk about the new tag we have use the <Text color={"blue.400"} as="b">{`<h1>`}</Text>.
            </Text>

            <Text mt={3}>
              This tag is really a series of tags and you can refer to them as heading tags and they go from h1 to h6 the higher the number the smaller the default text.
            </Text>
            <Text mt={3}>
              You can and should think of these as order of importance the h1 tag should contain the most important title, the h2 the second most important so on and so on. Though you can use multiple of each tag so for now don't worry about this too much. But try and build good habits now.
            </Text>

            <Text mt={3}>
              This on is important because as a gront end dev. This is one of the more common tags.
            </Text>

            <Text mt={3}>
              Next on the list is the <Text color={"blue.400"}> {`<div>`}</Text>tag again on of the more common tags. A mnemonic for this one would be a division.
              But in practical thinking this is a box that you put other things in we change the size and shape of this box as we need we can also change how the other tags are arranged in the box.
              As such this is going to be an essential building block.
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Flex >
  );
}
