import { Box, Container, Flex, Grid, GridItem, Heading, Text, Tooltip } from '@chakra-ui/react'
import { AnimatePresence } from 'framer-motion'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Section from '../components/section'
import Navbar from '../components/navbar'


export default function Tech() {

  return (
    <AnimatePresence>
      <Box className={styles.home} h='auto' minH='130vh' w='100%'>
        <Section>
          <Flex>
            <Navbar />
            <Flex mt='33vh' flexDir='column' justify={'center'} align={'center'} minW='100%' h='auto' >
              <Flex justify='space-around' minW='75%'>
                <Flex
                  flexDir='column'
                  justify='center'
                  align='center'
                  borderWidth={'2px'}
                  bg={'gray.800'}
                  borderBlockEndColor='green.400'
                  borderBlockStartColor={'blue.300'}
                  borderLeftColor={'blue.200'}
                  borderRightColor={'green.300'}
                  minW={'25vw'}
                  borderRadius='md'
                >
                  <Heading size='lg'>
                    Favorite Tech
                  </Heading>
                  <Flex
                    flexDir='column'
                    padding={2}
                    ml={2}
                  >
                    <Text>
                      Airlift sit/stand desk.
                    </Text>
                    <Text>
                      Pop.OS.
                    </Text>
                    Nuphy Air75 Keyboard
                    <Text>
                      AudioPro Mic.
                    </Text>
                    Logi Mx Master 2 mouse.
                    <Text>
                      Asus 32in moniter.
                    </Text>
                  </Flex>
                </Flex>
                <Flex
                  bg={'gray.800'}
                  borderBlockEndColor='green.400'
                  borderBlockStartColor={'blue.300'}
                  borderLeftColor={'blue.200'}
                  borderRightColor={'green.300'}
                  flexDir='column'
                  justify='center'
                  align='center'
                  borderWidth={'2px'}
                  borderColor={"aliceblue"}
                  minW={'25vw'}
                  borderRadius='md'
                >
                  <Heading size='lg' padding={2}>
                    Dev environment
                  </Heading>
                  <Flex
                    flexDir='column'
                    ml={2}
                    padding={2}
                  >
                    <Text>
                      Text Editor: vim/lunarvim
                    </Text>
                    <Text>
                      IDE: Jetbrains series IDE&apos;s
                    </Text>
                    <Text>
                      Browser: Brave
                    </Text>
                    <Text>
                      Terminal: Alacritty
                    </Text>
                    <Text>
                      Recording: OBS-studio
                    </Text>
                  </Flex>
                </Flex>
              </Flex>

              <Heading mt={5}>
                Technologies
              </Heading>
              <Grid
                templateColumns='repeat(5, 1fr)'
                minH={'45%'}
                minW={'80%'}
                gap={5}
                mt={5}
                justifyItems={'center'}
                alignItems={'center'}
              >
                <Tooltip label="Great frontend library for creating responsive UI&apos;s" >
                  <GridItem>
                    <Image src='/react.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='Huge Frontend Framework with tons of built in features reliant on typescript'>
                  <GridItem>
                    <Image src='/angular.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='My favorite so far React but better'>
                  <GridItem>
                    <Image src='/next.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='My first language back when I was teaching myself coding'>
                  <GridItem>
                    <Image src='/python.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='FastAPI like Flask but Fast'>
                  <GridItem>
                    <Image src='/FastAPI.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='Django full featured backend API framework for python'>
                  <GridItem>
                    <Image src='/django.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label='Very performate all around language'>
                  <GridItem>
                    <Image src='/csharp.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label="It's Everywhere">
                  <GridItem>
                    <Image src='/js.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
                <Tooltip label="It's Everywhere and typed">
                  <GridItem>
                    <Image src='/typescript.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
              </Grid>

              <Heading mt={5}>
                Learning
              </Heading>
              <Grid
                templateColumns='repeat(5, 1fr)'
                minH={'45%'}
                minW={'80%'}
                gap={5}
                mt={5}
                justifyItems={'center'}
                alignItems={'center'}
              >
                <Tooltip label="Kotlin it makes IDE's">
                  <GridItem>
                    <Image src='/Kotlin.png' height={100} width={100} />
                  </GridItem>
                </Tooltip>
              </Grid>
            </Flex>
          </Flex>
        </Section>
      </Box>
    </AnimatePresence>
  )
} 
