import type { NextPage } from "next";
import styles from "../styles/Home.module.css";
import { Flex } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useState } from "react";
import Landing from "../components/landing";


const Home: NextPage = () => {
  const [landing, setLanding] = useState(true);
  const router = useRouter();

  function finishWelcome() {
    setLanding(false);
    router.push('/main-page')
  }

  return (
    <Flex className={styles.home} flexDir="column" alignItems="center">
      {landing && <Landing setLanding={finishWelcome} />}
    </Flex>
  );
};

export default Home;
