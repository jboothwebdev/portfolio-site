import { extendTheme } from "@chakra-ui/react"


const color = extendTheme({
  initialColorMode: 'dark',
  useSystemColorMode: ' false',
  styles: {
    global: (props) => ({

      body: {
        fontFamily: 'Ubuntu, sans-serif',
        color: 'blue.50',
        background: 'black'
      },
    })
  },
  fonts: {
    heading: 'Ubuntu, sans-serif',
    body: 'Ubuntu, sans-serif'
  }
})

const theme = extendTheme({ color })

export default theme;
