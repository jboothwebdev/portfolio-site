import { Flex } from "@chakra-ui/react";
import { motion, AnimatePresence } from "framer-motion";
import { useState } from "react";
import styles from "../styles/Landing.module.css";
export default function Landing(props: any) {
  let line1 = "export default function greeting(){";
  let line2 = 'console.log("Create Through Code");';
  let line3 = "}";
  let [visible, setVisible] = useState(true);
  let [line1Vis, setLine1Vis] = useState(false);
  let [line2Vis, setLine2Vis] = useState(false);
  let [line3Vis, setLine3Vis] = useState(false);
  const text = {
    hidden: { opacity: 1 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.5,
      },
    },
  };
  setTimeout(() => {
    triggerLine1();
  }, 500);

  const triggerLine1 = () => {
    setLine1Vis(true);
    setTimeout(() => {
      triggerLine2();
    }, 1500);
  };
  const triggerLine2 = () => {
    setLine2Vis(true);
    setTimeout(() => {
      triggerLine3();
    }, 1500);
  };
  const triggerLine3 = () => {
    setLine3Vis(true);
    // TODO trigger the animation to go to the home page
    setTimeout(() => {
      setVisible(false);
    }, 700);
    setTimeout(() => {
      props.setLanding(false);
    }, 2000);
  };
  // @ts-ignore
  return (
    <Flex
      flexDir="column"
      align="center"
      justify="center"
      h="100vh"
      w="100vw"
      bg="gray.800"
    >
      <AnimatePresence>
        {visible ? (
          <motion.div variants={text} initial="hidden" animate="show">
            {line1Vis && (
              <div className={styles.typewriter}>
                <h1 className={styles.line1}>{line1}</h1>
              </div>
            )}

            {line2Vis && (
              <div className={styles.typewriter}>
                <h1 className={styles.line2}>{line2}</h1>
              </div>
            )}
            {line3Vis && (
              <div className={styles.typewriter}>
                <h1 className={styles.line2}>{line3}</h1>
              </div>
            )}
          </motion.div>
        ) : (
          <motion.div
            initial={{ opacity: 1 }}
            animate={{ opacity: 0 }}
            transition={{ duration: 2 }}
            style={{
              height: "100vh",
              width: "100vw",
              backgroundColor: "white",
            }}
          ></motion.div>
        )}
      </AnimatePresence>
    </Flex>
  );
}
