import Image from 'next/image'
export default function Banner() {
  return (
    <Image alt='voxel art desk' width={640} height={400} sizes='25vw' src='/deskart.png' layout='intrinsic' />
  )
}
