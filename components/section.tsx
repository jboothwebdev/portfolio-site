import { chakra, shouldForwardProp } from "@chakra-ui/react";
import { motion } from "framer-motion";

const StyledDiv = chakra(motion.div, {
  shouldForswardProp: (prop: any) => {
    return shouldForwardProp(prop) || prop === "t";
  },
});
//@ts-ignore
const Section = ({ children }) => (
  <StyledDiv
    initial={{ y: 10, opacity: 0 }}
    animate={{ y: 0, opacity: 1 }}
    //@ts-ignore
    transition={{ duration: 0.8, delayChildren: 10 }}
  >
    {children}
  </StyledDiv>
);

export default Section;
