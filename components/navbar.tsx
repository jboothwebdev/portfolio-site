import { Box, Button, Flex } from "@chakra-ui/react"
import { useRouter } from 'next/router';
import Image from 'next/image'

export default function Navbar() {
  const router = useRouter();

  return (
    <Flex position='absolute' width={'97%'} zIndex='9999' bg='transparent' justify={'space-between'} top={6}>
      <Box>
        <Button bg='transparent' onClick={() => { router.push('/main-page') }}>Jeremiah Booth <Image src='/laptopIcon.png' height={50} width={50} alt={"laptop"} /> </Button>
      </Box>
      <Flex >
        <Button mr={2} bgGradient='linear(
          to-r,
          teal.200,
          blue.200)'
          color={'gray.700'}
          onClick={() => { router.push('/tech') }}
        >Tech
        </Button>
        <Button bgGradient='linear(
          to-r,
          teal.200,
          blue.200)'
          color={'gray.700'}
          onClick={() => {router.push('/tutorials/home')}}
        >
          Tutorials
        </Button>
      </Flex>
    </Flex >
  )
}
