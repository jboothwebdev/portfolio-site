import {Divider, Flex, Heading, Link} from "@chakra-ui/react";

export default function TutorialSidebar(){
  return (
    <Flex
      flexDir={"column"}
      minW={"15%"}
      minH={"96vh"}
      bgColor={"cyan.900"}
    >
      <Heading size={'lg'}>Tutorials</Heading>
      <Divider/>
      <Heading ml={3} size={'md'}> Web development:</Heading>
      <Link ml={10} href='/tutorials/web-development/lessonOne'> Lesson One </Link>
    </Flex>
  )
}
