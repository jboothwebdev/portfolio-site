import { GridItem, Grid, Flex, Heading, Text } from "@chakra-ui/react";
import Image from 'next/image';

export default function About() {
  const imageStyle = {
    borderRadius: '999px',
  }
  return (
    <Grid
      templateColumns="repeat(6,1fr)"
      templateRows="repeat(3, 1fr)"
      gap={10}
      h="auto"
      w="95%"
      mt={10}
      justifyContent="center"
      alignItems="center"
    >
      <GridItem
        rowSpan={1}
        colSpan={4}
        colStart={2}
        h="auto"
        color="gray.100"
        p={3}
      >
        <Flex justifyContent="center" alignItems="end" h="100%">
          Man is most free when his tools are proportionate to his needs.
          -Soetsu Yanagi
        </Flex>
      </GridItem>
      <GridItem
        rowSpan={2}
        rowStart={2}
        colStart={3}
        colSpan={2}
        alignItems='end'
      >
        <Image src="/20220401_135735.jpg" layout='intrinsic' height={240} width={200} objectFit='scale-down' alt={"laptop"} />
      </GridItem>
      <GridItem rowSpan={3} rowStart={2} colStart={4} colSpan={2}>
        <Heading as="h3" mb={2} fontSize={"2xl"}>
          Jeremiah Booth
        </Heading>
        <Text>
          Jeremiah is a full stack developer living in Arizona. Jeremiah
          has worked in various levels of code bases from small personal projects to enterprise level monorepos
          and enjoys being able to
          work in all aspects of the development projects. When he is not at
          work he likes to study various topics as well as photography, spending time with family, and enjoying  and relaxing outdoors.{" "}
        </Text>
      </GridItem>
    </Grid>
  );
}
