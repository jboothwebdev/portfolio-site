import { Container, Flex, Grid, Heading, Text } from '@chakra-ui/react';
import Image from 'next/image';
import { useRouter } from 'next/router'

// TODO: Add photo thumbnails to the containers 
export default function Projects() {
  const router = useRouter()
  return (
    <Flex
      flexDir='column'
      width='80%'
      justify='center'
      alignItems='center'
      ml={5}
    >
      <Heading mt={30}>
        Projects
      </Heading>
      <Flex
        flexDir={'row'}
        justify={'space-around'}
        align={'center'}
        mb={10}
      >
        <Container
          w='35vw'
          h='30vh'
          display='flex'
          flexDir='column'
          justifyContent={'center'}
          alignItems={'center'}
          mt={4}
          bg={'gray.800'}
          borderBlockEndColor='green.400'
          borderBlockStartColor={'blue.300'}
          borderLeftColor={'blue.200'}
          borderRightColor={'green.300'}
          borderWidth='2px'
          borderRadius={5}
          boxShadow='md, blue.300'
          onClick={() => { router.push('/onBudget') }}
        >
          <Heading fontSize='2xl' cursor='pointer'>
            on Budget
          </Heading>
          <Image src='/dashboard.png' height={200} width={260} />
          <Text cursor='pointer'>
            Simple monthly budget application.
          </Text>
          <Text cursor='pointer'>
            Click to read more
          </Text>
        </Container>
      </Flex>
    </Flex>
  )
}


