import {
  Button,
  Flex, IconButton, Box
} from "@chakra-ui/react";
import { HamburgerIcon} from "@chakra-ui/icons";
import {useRouter} from "next/router";
import Image from 'next/image'
import React from "react";

interface IProps {
  setSideBar: CallableFunction;
}

export default function TutorialNavbar(props: IProps) {
  const router = useRouter();

  return (
    <Flex 
      position='absolute'
      zIndex={999}
      width={"100%"}
      height={'4vh'}
      bgColor={"cyan.800"}
      align={"center"}
    >
      <IconButton
        variant='outline'
        colorScheme='white'
        aria-label='Send email'
        icon={<HamburgerIcon />}
        m={2}
        onClick={props.setSideBar()}
      />
      <Box>
        <Button bg='transparent' onClick={() => { router.push('/main-page') }}>Jeremiah Booth <Image src='/laptopIcon.png' height={50} width={50} alt={"laptop"}/> </Button>
      </Box>
    </Flex>
  )
}
