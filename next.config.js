/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['i9.ytimg.com']
  }
}

module.exports = nextConfig 
